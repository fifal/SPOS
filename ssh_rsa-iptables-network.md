# SSH
Pro vygenerování nového ssh RSA 2048 klíče:
```bash
ssh-keygen -t rsa
# Pokud se nenastaví cesta, defaultně se uloži do:
#   $HOME/.ssh/id_rsa (privátní klíč)
#   $HOME/.ssh/id_rsa.pub (veřejný klíč)
```
dále je třeba nakopírovat veřejný klíč na server: 
```bash
ssh-copy-id root@server.cz
# Pokud byl klíč uložen do jiné složky:
#   ssh-copy-id -i /cesta/ke/klici root@server.cz
```
pokud nefunguje příkaz **ssh-copy-id** je možné přidat veřejný klíč na server ručně. Pokud neexistuje soubor **/root/.ssh/authorized_keys** vytvořit a nakopírovat tam obsah id_rsa.pub.

Vypnutí přihlašování pomocí hesla, do souboru **/etc/ssh/sshd_config** přidat:
**PasswordAuthentication no** a provést restart ssh:
```bash
service sshd restart
```

# Nastavení network interface
Nastavení statické ip adresy v souboru **/etc/network/interfaces**, tyhle nastavení se načítaj při rebootu a tak:
```
##The primary network interface
##iface eth0 inet dhcp
auto eth0
iface eth0 inet static
        address 10.0.0.5 #nastavení statické IP
        netmask 255.255.255.0 #maska
        gateway 10.0.0.1 #brána

##Virtual ip        
auto eth0:1
iface eth0:1 inet static
  address 192.168.1.248
  netmask 255.255.255.0
```
po nastavení třeba restartovat networking: 
```bash
service networking restart
```

# Firewall - iptables
```bash
iptables -L -n #vypíše aktivní pravidla
iptables -A INPUT -s 147.228.0.0/16 -p tcp -m tcp --dport 22 -j ACCEPT #povolení přístupu na port 22 ze sítě 147.228.0.0/16
iptables -A INPUT -p tcp -m tcp --dport 22 -j DROP #zakázání připojení z ostatních sítí na port 22
```

Po restartu se ovšem pravidla smažou, je potřeba je uložit a upravit /etc/network/interfaces + script:
```bash
iptables-save > /etc/network/iptables.conf #uloží pravidla z iptables do souboru iptables.conf
```
u nějakého interfaceu v **/etc/network/interfaces** přidat položku **post-up**:
```
iface eth0 inet static 
       post-up /sbin/iptables-restore /etc/network/iptables.conf #pomocí iptables-restore obnoví iptables
```

Každou úpravu ve firewallu je poté možné napsat do /etc/network/iptables.conf a restartovat síť
smazání pravidla se dělá místo -A se dá -D, nebo iptables --flush (který smaže všechny pravidla)





