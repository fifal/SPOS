# Apache
## Instalace
```bash
apt-get install apache2 #samotný apache
apt-get install libapache2-mod-php5 #podpora php5, případně install php7.0
apt-get install php5-mysql #(obsahuje mysqli modul do php)
service apache2 restart
```
## Nastavení PHP
V souboru **/etc/php5/apache2/php.ini** nastavíme kvůli bezpečnosti přístupu php:
**open_basedir = /var/www/**

Pokud nějaký VH potřebuje přístup k jiné složce, přidáme do VH.conf souboru:
**php_admin_value open_basedir /var/www/:/home/extrafolder/**

Ve stejném souboru zakážeme funkci phpinfo() tím, že přidáme do parametru:
**disable_functions = ...,phpinfo,**

## Nastavení apache
Nastavení apache je v souboru **/etc/apache2/apache2.conf**

Přidání dalšího VH ve složce **/etc/apache2/sites-available**... je potřeba nastavit ServerName, RootDirectory a logovací soubory:
```bash
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/host2.conf
a2ensite host2.conf #povolení nového VHosta
service apache2 reload #reload apache
```

### SSL
Pro SSL je potřeba zapnout moduly:
```bash
a2enmod ssl
a2enmod headers
```
Ve složce **/etc/apache2/** vytvoříme složku "ssl" a v něm certifikát:
```bash
openssl genrsa -des3 -out server.key 1024
openssl req -new -key server.key -out server.csr
```

Odebereme nutnost zadat klíč při rebootování apache:
```bash
cp server.key server.key.org
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
```

Musíme přidat nový VHost pro ssl a ve starém nastavit přesměrování na zabezpečený. Pro přesměrování se použije *Redirect /*
```XML
<VirtualHost *:80>
    ServerName secure.spos-name.spos
    Redirect / https://secure.spos-name.spos/
</VirtualHost>
```

Vytvoříme nový VH *secure.conf* s přidanými cestami k certifikátu:
```XML
<VirtualHost *:443>
    ServerName secure.spos-name.spos
    DocumentRoot "/var/www/secure/"

    SSLEngine on
    SSLCertificateFile /etc/apache2/ssl/server.crt
    SSLCertificateKeyFile /etc/apache2/ssl/server.key

    ErrorLog /var/log/apache2/secure-error.log
    CustomLog /var/log/apache2/secure-access.log common
</VirtualHost>
```

## Naslouchající port
Soubor **/etc/apache2/ports.conf**, lze do něj zapsat naslouchající ip adresy a porty
Listen 192.168.255.55:8888 (dovoluje virtuálům naslouchat na konkrténí adrese a portu), kromě toho se musí na rozhraní přidat daná IP, pokud neexistuje:

```bash
ip addr add 192.168.255.55/24 dev eth0 #přidá na rozhraní eth0 ip adresu 192.168.255.55 s maskou /24
ip addr show dev eth0 #zobrazí IP které jsou na daném rozhraní
```

# Apachetop
Sledování stavu apache
Instalace a použití apachetop:

```bash
apt install apachetop
apachetop -f /var/log/apache2/access.log #logovací soubor konkrétního VH
```

Vytvoření umělého provozu pomocí nástroje curl (./test http://spos-name.spos 0.001):
(jenom pro ukázku funkčnosti apachetop)
```bash
#!/bin/bash
get_page () {
        echo "request "$2""
        curl -k "$1" &>/dev/null
}
i=0
while true
do
        ((i=i+1))
        get_page $1 $i
        sleep $2
done
```







